import { Adapter } from '@sveltejs/kit';
export interface IAnagolayAdapterOptions {
    fallback?: string;
    appType: 'spa' | 'static';
    subdomain?: string;
    precompress?: boolean;
    address: string;
}
/**
 * An interface for the routes
 */
export interface IRoute {
    src: string;
    headers?: Record<string, any>;
    status?: 307 | 308;
}
/**
 * The keys correspond to the `trailingSlash` sveltekit config
 */
export interface IRoutes {
    always: IRoute[];
    never: IRoute[];
    ignore: IRoute[];
}
/**
 *
 * @param incomingOption -
 * @returns
 */
export default function (incomingOption: IAnagolayAdapterOptions): Adapter;
