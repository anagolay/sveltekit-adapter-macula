import { Adapter, Builder } from '@sveltejs/kit';
import { readFileSync } from 'fs';
import { join } from 'path';
import { equals, isEmpty, isNil, mergeRight } from 'ramda';
import { fixHtmlDocument, write } from './utils.js';

export interface IAnagolayAdapterOptions {
	fallback?: string;
	appType: 'spa' | 'static';
	subdomain?: string;
	precompress?: boolean;
	address: string
}


/**
 * An interface for the routes
 */
export interface IRoute {
	src: string;
	headers?: Record<string, any>;
	status?: 307 | 308;
}
/**
 * The keys correspond to the `trailingSlash` sveltekit config
 */
export interface IRoutes {
	always: IRoute[];
	never: IRoute[];
	ignore: IRoute[];
}

/**
 * Shamelessly taken from sveltekit vercel adapter. It makes a lot of sense and why re-invent the wheel
 */
// rules for clean URLs and trailing slash handling,
// generated with @vercel/routing-utils
const redirects: IRoutes = {
	always: [
		{
			/**
			 * redir only
			 * ```
			 * /dir/index -> /dir/
			 * /dir/index/ -> /dir/
			 * ```
			 */
			src: '^/(?:(.+)/)?index(?:\\.html)?/?$', // https://regex101.com/r/OgOahm/1
			headers: {
				Location: '/$1/'
			},
			status: 308
		},
		{
			src: '^/(.*)\\.html/?$', // https://regex101.com/r/OUMx3x/1
			headers: {
				Location: '/$1/'
			},
			status: 308
		},
		{
			src: '^/\\.well-known(?:/.*)?$' // matches `/\.well-known`
		},
		{
			src: '^/((?:[^/]+/)*[^/\\.]+)$', // match `/features/feature-a` will redirect to `/features/feature-a/`
			headers: {
				Location: '/$1/'
			},
			status: 308
		},
		{
			src: '^/((?:[^/]+/)*[^/]+\\.\\w+)/$',
			headers: {
				Location: '/$1'
			},
			status: 308
		}
	],
	never: [
		{
			src: '^/(?:(.+)/)?index(?:\\.html)?/?$',
			headers: {
				Location: '/$1'
			},
			status: 308
		},
		{
			src: '^/(.*)\\.html/?$',
			headers: {
				Location: '/$1'
			},
			status: 308
		},
		{
			src: '^/(.*)/$',
			headers: {
				Location: '/$1'
			},
			status: 308
		}
	],
	ignore: [
		{
			src: '^/(?:(.+)/)?index(?:\\.html)?/?$',
			headers: {
				Location: '/$1'
			},
			status: 308
		},
		{
			src: '^/(.*)\\.html/?$',
			headers: {
				Location: '/$1'
			},
			status: 308
		}
	]
};

/**
 *
 * @param incomingOption -
 * @returns
 */
export default function (incomingOption: IAnagolayAdapterOptions) {
	const adapter: Adapter = {
		name: 'sveltekit-adapter-macula',
		async adapt(builder: Builder) {

			const { address, appType, fallback, subdomain, precompress } = incomingOption


			if (isNil(address) || isEmpty(address)) {
				builder.log.error('address must be set to any subtrate based address')
				return
			}
			const kitConfig = builder.config.kit;
			const dir = `.macula`;

			const tmp = builder.getBuildDirectory('macula-tmp');

			builder.rimraf(dir);
			builder.rimraf(tmp);

			//merge with defaults
			const options = mergeRight({
				appType: 'static'
			}, incomingOption);


			const pages = `${dir}`;
			const assets = `${dir}`;

			builder.log(`Processing ${appType}`);

			let fallbackInternal = undefined;

			if (equals(appType, 'static')) {
				if (!equals(kitConfig.trailingSlash, 'always')) {
					builder.log.error(
						'trailingSlash is not set to always. Static sites MUST have trailingSlash=always'
					);
					return;
				}
				if (!equals(kitConfig.prerender.default, true)) {
					builder.log.error(
						'prerender.default is not set to true. Static sites MUST  prerender all pages'
					);
					return;
				}
			} else if (equals(appType, 'spa')) {
				if (equals(kitConfig.prerender.default, true)) {
					builder.log.warn(
						`prerender.default is set to true. You might check your code for pages you don't want to prerender.`
					);
				}
				if (isNil(fallback)) {
					fallbackInternal = 'index.html';
				}
			} else {
				throw new Error(`This appType is not supported: ${appType}`);
			}

			builder.writeClient(assets);
			builder.writePrerendered(pages, { fallback: fallbackInternal });

			const prerenderedRedirects = Array.from(builder.prerendered.redirects, ([src, redirect]) => ({
				src,
				headers: {
					Location: redirect.location
				},
				status: redirect.status
			}));


			const routes = [
				...redirects[builder.config.kit.trailingSlash], // << not using this for the time being. brings complexity
				...prerenderedRedirects,
				{
					src: `/${builder.config.kit.appDir}/immutable/.+`,
					headers: {
						'cache-control': 'public, immutable, max-age=31536000'
					}
				}
			];

			const prerenderedPages: Record<string, { file: string }> = {};
			console.log(builder.prerendered.pages);

			/**
			 * We are doing follwing here:
			 * 1. setting proper routes
			 * 2. setting prerenderedPages
			 * 3. making the links relative to the /
			 */
			// if (!equals(builder.config.kit.trailingSlash, 'always')) {
			for (const [src, page] of builder.prerendered.pages) {

				// routes.push({
				// 	src: src,
				// 	headers: {
				// 		Location: src
				// 	},
				// 	status: 308
				// });

				// implicit redirects (trailing slashes)
				// if (src !== '/') {
				// 	routes.push({
				// 		src: src,
				// 		headers: {
				// 			Location: src
				// 		},
				// 		status: 308
				// 	});
				// } else {

				// explicit prerenderedPages, maybe not needed ???
				// prerenderedPages[page.file] = { path: src };
				prerenderedPages[src] = { file: '/' + page.file };
				// }

				/**
				 * this part gets overwritten for static pages
				 */
				// // building the relative paths
				// const indexPath = join(pages, page.file);
				// const indexContent = readFileSync(indexPath).toString();
				// // return all but first and lst element which will always be '' if matched and string starts with /. the routes come in as /route1/
				// const fileDepthParts = init(tail(split('/')(src)));

				// // actually fix the html document
				// const newContent = fixHtmlDocument(indexContent, length(fileDepthParts));
				// write(indexPath, newContent);
			}
			// }



			// if (!isNil(fallback)) {
			// 	const indexPath = join(pages, fallback);
			// 	const indexContent = readFileSync(indexPath).toString();
			// 	const newContent = fixHtmlDocument(indexContent, 0);
			// 	write(indexPath, newContent);
			// }

			if (precompress) {
				builder.log.minor('Compressing assets');
				builder.compress(pages);

				builder.log.minor('Compressing pages');
				builder.compress(pages);
			}

			type AvailableCompressions = 'br' | 'gz'

			interface MaculaConfig {
				version: 1;
				source: 'sveltekit'
				preredered: boolean
				appType: 'spa' | 'static';
				fallback?: {
					file?: string;
					route?: string;
				};
				compressedFor: AvailableCompressions[]
				routes: any[];
				pages: Record<string, { file: string }>,
				address: string,
				subdomain?: string
			}

			const maculaJson: MaculaConfig = {
				version: 1,
				source: 'sveltekit',
				preredered: builder.config.kit.prerender.default,
				appType,
				fallback: {
					file: fallbackInternal,
					route: '/'
				},
				compressedFor: ['gz', 'br'],
				routes,
				pages: prerenderedPages,
				address: `urn:anagolay:${address}`,
				subdomain
			};
			write(`${dir}/macula.json`, JSON.stringify(maculaJson, null, 2));
		}
	};

	return adapter;
}
