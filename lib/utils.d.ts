/**
 * Create missing dirs and then write a file
 * @param file -
 * @param data -
 */
export declare function write(file: string, data: string): void;
/**
 * This will be repeated as many times as needed to get to the top
 */
export declare const baseDepthDoubleDots: string;
/**
 *
 */
export declare const baseDepthSingleDot: string;
export declare function fixHtmlDocument(content: string, relativeDepthToRoot: number): string;
